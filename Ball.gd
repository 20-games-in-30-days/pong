extends CharacterBody2D

@export var start_speed := 800
@export var paddle_angle_factor := 10

var vel := Vector2.ZERO


func _ready():
	vel.x = start_speed


func flip():
	start_speed = -start_speed


func _physics_process(_delta):
	set_velocity(vel)
	set_up_direction(Vector2.UP)
	move_and_slide()
	var _v = vel
	if is_on_ceiling() or is_on_floor():
		vel.y = -vel.y
		$Wall.play()
		
	if is_on_wall():
		vel.x = -vel.x
		$Paddle.play()
		for slide in get_slide_collision_count():
			var collision = get_slide_collision(slide)
			# This should just be the paddle.
			vel.y = (transform.origin.y - collision.get_collider().position.y) * paddle_angle_factor
