extends CharacterBody2D

@export var player := 1
@export var speed := 800

func _physics_process(_delta):
	var vel = Vector2.ZERO
	
	if player == 1:
		if Input.is_action_pressed("p1_up"):
			vel.y = -speed * Input.get_action_strength("p1_up")
		elif Input.is_action_pressed("p1_down"):
			vel.y = speed * Input.get_action_strength("p1_down")
	elif player == 2:
		if Input.is_action_pressed("p2_up"):
			vel.y = -speed * Input.get_action_strength("p2_up")
		elif Input.is_action_pressed("p2_down"):
			vel.y = speed * Input.get_action_strength("p2_down")
	
	set_velocity(vel)
	move_and_slide()
	vel = vel
