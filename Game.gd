extends CanvasLayer


var ball_prefab = preload("res://Ball.tscn")
var ball
var ball_default_pos := Vector2(800, 475)
var score = Vector2(0, 0)


func _ready():
	ball = ball_prefab.instantiate()
	add_child(ball)


func reset():
	ball.queue_free()
	ball = ball_prefab.instantiate()
	call_deferred("add_child", ball)
	$End.play()
	

func _on_EdgeL_body_entered(_body):
	score.x += 1
	$UI/Score_2.text = "%03d" % score.x
	reset()
	ball.flip()


func _on_EdgeR_body_entered(_body):
	score.y += 1
	$UI/Score_1.text = "%03d" % score.y
	reset()
